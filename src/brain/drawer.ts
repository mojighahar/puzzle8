import Tree, { Node } from "./Tree";
import { Point } from "./types";

export default class TreeDrawer {
  canvas: HTMLCanvasElement;
  context: CanvasRenderingContext2D;
  tree: Tree;
  gutter: number = 10;

  constructor(canvas: HTMLCanvasElement, tree: Tree, gutter: number = 10) {
    this.canvas = canvas;
    const context = canvas.getContext("2d");
    if (context === null) throw new Error("Context is null");
    this.context = context;
    this.tree = tree;
    this.gutter = gutter;
  }

  draw() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    const treeSize = this.tree.size();
    const nodeSize = this.canvas.height / treeSize.height;
    this.drawNode(this.tree.rootNode, nodeSize, 0, 0, this.canvas.width);
  }

  drawNode(
    node: Node,
    size: number,
    top: number,
    left: number,
    domain: number
  ) {
    this.context.beginPath();
    const radius = (size - this.gutter) / 2;

    const centerPosition = {
      x: left + domain / 2,
      y: top + size / 2,
    };

    this.context.save();
    let fontSize = size / 2;
    this.context.font = fontSize + "px sans-serif";
    if (node.active) {
      this.context.fillStyle = "#393";
      this.context.strokeStyle = "#393";
    }
    this.context.ellipse(
      centerPosition.x,
      centerPosition.y,
      radius,
      radius,
      0,
      0,
      360
    );
    this.context.stroke();
    this.context.closePath();
    let textSize = this.context.measureText(node.content);
    while (textSize.width > size) {
      fontSize -= 5;
      this.context.font = fontSize + "px sans-serif";

      textSize = this.context.measureText(node.content);
    }
    this.context.fillText(
      node.content,
      centerPosition.x + textSize.width / 2,
      centerPosition.y + textSize.fontBoundingBoxDescent
    );

    this.context.restore();

    let offset = 0;

    node.children.forEach((child) => {
      const domainRatio = child.width() / node.width();
      const childDomain = domain * domainRatio;
      const childDomainPosition = {
        left: left + offset,
        top: top + size,
      };
      this.drawEdge(
        { x: centerPosition.x, y: centerPosition.y },
        {
          x: childDomainPosition.left + childDomain / 2,
          y: childDomainPosition.top + size / 2,
        },
        size
      );
      this.drawNode(
        child,
        size,
        childDomainPosition.top,
        childDomainPosition.left,
        childDomain
      );
      offset += childDomain;
    });
  }

  drawEdge(center1: Point, center2: Point, size: number) {
    const difference = {
      x: center2.x - center1.x,
      y: center2.y - center1.y,
    };

    const distance = Math.sqrt(
      Math.pow(difference.x, 2) + Math.pow(difference.y, 2)
    );

    const cos = difference.x / distance;
    const sin = difference.y / distance;

    const offset = {
      x: ((size - this.gutter) / 2) * cos,
      y: ((size - this.gutter) / 2) * sin,
    };

    this.context.beginPath();
    this.context.moveTo(center1.x + offset.x, center1.y + offset.y);
    this.context.lineTo(center2.x - offset.x, center2.y - offset.y);
    this.context.stroke();
    this.context.closePath();
  }
}
