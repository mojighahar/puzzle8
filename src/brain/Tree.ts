let lastId = -1;

export class Node {
  id: number;
  content: string;
  children: Node[];
  active: boolean;

  constructor(content: string, active: boolean = false, children: Node[] = []) {
    this.id = ++lastId;
    this.content = content;
    this.children = children;
    this.active = active;
  }

  addChild(node: Node) {
    this.children.push(node);
  }

  width(): number {
    if (this.children.length === 0) {
      return 1;
    }

    return this.children.reduce((size, node) => size + node.width(), 0);
  }

  height(): number {
    if (this.children.length === 0) {
      return 1;
    }

    return Math.max(...this.children.map((child) => child.height())) + 1;
  }

  clone(): Node {
    const node = new Node(
      this.content,
      this.active,
      this.children.map((child) => child.clone())
    );
    node.id = this.id;
    return node;
  }
}

export default class Tree {
  rootNode: Node;

  constructor(node: Node) {
    this.rootNode = node;
  }

  size() {
    return {
      width: this.rootNode.width(),
      height: this.rootNode.height(),
    };
  }

  find(nodeId: number) {
    return this.findInNode(this.rootNode, nodeId);
  }

  findInNode(node: Node, nodeId: number): Node | null {
    if (node.id === nodeId) {
      return node;
    }

    for (const childNode of node.children) {
      const result = this.findInNode(childNode, nodeId);
      if (result) return result;
    }

    return null;
  }

  clone() {
    const node = this.rootNode.clone();
    const tree = new Tree(node);
    return tree;
  }
}
