import Puzzle from "../puzzle";
import Tree, { Node } from "../Tree";
import { Report, StepCallback } from "../types";
import { DLS } from "./dls";

export default async function IDS(
  puzzle: Puzzle,
  maxDepth: number,
  stepCallback: StepCallback,
  searchTree?: Tree
) {
  for (let limit = 0; limit <= maxDepth; limit++) {
    if (
      await DLS(puzzle, limit, stepCallback, searchTree, searchTree?.rootNode)
    ) {
      return true;
    }
  }
  return false;
}
