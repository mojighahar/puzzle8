import Puzzle, { DirectionText } from "../puzzle";
import Tree, { Node } from "../Tree";
import { StepCallback } from "../types";

export async function DLS(
  puzzle: Puzzle,
  limit: number,
  stepCallback: StepCallback,
  searchTree?: Tree,
  searchTreeNode?: Node | null
) {
  console.log(searchTree);
  await stepCallback(puzzle, searchTree);
  if (puzzle.isTarget()) return true;

  if (limit <= 0) return false;

  for (const direction of puzzle.validMoves()) {
    const newPuzzle = puzzle.clone();
    let newSearchTree;
    if (searchTree) newSearchTree = searchTree.clone();
    let newSearchTreeNode;
    if (newSearchTree && searchTreeNode) {
      newSearchTreeNode = new Node(DirectionText[direction]);
      newSearchTree.find(searchTreeNode.id)?.addChild(newSearchTreeNode);
    }
    newPuzzle.move(direction);
    if (
      await DLS(
        newPuzzle,
        limit - 1,
        stepCallback,
        newSearchTree,
        newSearchTreeNode
      )
    ) {
      return true;
    }
  }
  return false;
}
