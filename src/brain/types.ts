import Puzzle from "./puzzle";
import Tree from "./Tree";

export type StepCallback = (
  puzzle: Puzzle,
  searchTreeNode?: Tree
) => Promise<void>;

export type Report = {
  name: string;
  value: string;
};

export type Point = {
  x: number;
  y: number;
};
