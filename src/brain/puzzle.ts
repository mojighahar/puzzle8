export enum Direction {
  UP,
  DOWN,
  Left,
  Right,
}

const DirectionMove = {
  [Direction.UP]: { row: -1, column: 0 },
  [Direction.DOWN]: { row: 1, column: 0 },
  [Direction.Left]: { row: 0, column: -1 },
  [Direction.Right]: { row: 0, column: 1 },
};

export const DirectionText = {
  [Direction.UP]: "UP",
  [Direction.DOWN]: "DOWN",
  [Direction.Left]: "LEFT",
  [Direction.Right]: "RIGHT",
};

export default class Puzzle {
  columns: number = 3;
  rows: number = 3;
  target: (string | null)[][];
  state: (string | null)[][] = [];

  constructor(target: any[][]) {
    this.target = target;
    this.validateTarget();
    this.rows = target.length;
    const { columns, rows } = this.targetSize();
    this.columns = columns;
    this.rows = rows;
    this.state = target.map((row) => [...row]);
  }

  shuffle() {
    const cellCount = this.columns * this.rows;
    const arrange = [];
    while (arrange.length !== cellCount) {
      const cellNumber = Math.floor(Math.random() * cellCount);
      if (arrange.indexOf(cellNumber) === -1) {
        arrange.push(cellNumber);
      }
    }
    const newState = new Array(this.rows)
      .fill(0)
      .map(() => new Array(this.columns).fill(0));
    arrange.forEach((cellNumber, index) => {
      const stateRow = Math.floor(index / this.columns);
      const stateColumn = index % this.columns;
      const targetRow = Math.floor(cellNumber / this.columns);
      const targetColumn = cellNumber % this.columns;

      newState[stateRow][stateColumn] = this.target[targetRow][targetColumn];
    });
    this.state = newState;
  }

  validateTarget() {
    const nullCount = this.target.reduce(
      (nullCount, row) =>
        nullCount + row.filter((cell) => cell === null).length,
      0
    );
    if (nullCount !== 1) {
      throw new Error("Target must has one and only one null value");
    }
  }

  targetSize() {
    const rowLengths = this.target.map((row) => row.length);
    const maxCells = Math.max(...rowLengths);
    const minCells = Math.min(...rowLengths);

    if (maxCells !== minCells) {
      throw new Error("Target has different row lengths");
    }

    return {
      columns: maxCells,
      rows: this.target.length,
    };
  }

  getPosition(content: string | null): { row: number; column: number } {
    for (let row = 0; row < this.state.length; row++) {
      for (let column = 0; column < this.state[row].length; column++) {
        if (this.state[row][column] === content) return { row, column };
      }
    }

    throw new Error("null cell not found");
  }

  validMoves(): Direction[] {
    const nullPosition = this.getPosition(null);

    const validMoves = [];

    if (nullPosition.row > 0) {
      validMoves.push(Direction.UP);
    }
    if (nullPosition.row < this.rows - 1) {
      validMoves.push(Direction.DOWN);
    }
    if (nullPosition.column > 0) {
      validMoves.push(Direction.Left);
    }
    if (nullPosition.column < this.columns - 1) {
      validMoves.push(Direction.Right);
    }

    return validMoves;
  }

  move(direction: Direction) {
    if (!this.validMoves().includes(direction))
      throw new Error("invalid direction");

    const nullPosition = this.getPosition(null);

    const target = {
      row: nullPosition.row + DirectionMove[direction].row,
      column: nullPosition.column + DirectionMove[direction].column,
    };

    const newState = this.state.map((row) => [...row]);

    newState[nullPosition.row][nullPosition.column] =
      newState[target.row][target.column];

    newState[target.row][target.column] = null;
    this.state = newState;
  }

  isTarget() {
    for (let row = 0; row < this.rows; row++) {
      for (let column = 0; column < this.columns; column++) {
        if (this.state[row][column] !== this.target[row][column]) {
          return false;
        }
      }
    }

    return true;
  }

  reset() {
    this.state = this.target;
  }

  clone() {
    const puzzle = new Puzzle(this.target);
    puzzle.state = this.state.map((row) => [...row]);
    return puzzle;
  }
}
